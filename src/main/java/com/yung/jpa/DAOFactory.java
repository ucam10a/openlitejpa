package com.yung.jpa;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.persistence.EntityListeners;
import javax.sql.DataSource;

public class DAOFactory {

    public static <C extends AbstractDAO<T>, T extends BasicEntity> C getDAO(Class<T> cls, Connection conn) throws Exception{
        return getDAO(cls, null, conn, null);
    }
    
    public static <C extends AbstractDAO<T>, T extends BasicEntity> C getDAO(Class<T> cls, Connection conn, Object printer) throws Exception{
        return getDAO(cls, null, conn, printer);
    }
    
    public static <C extends AbstractDAO<T>, T extends BasicEntity> C getDAO(Class<T> cls, DataSource ds) throws Exception{
        return getDAO(cls, ds, null, null);
    }
    
    public static <C extends AbstractDAO<T>, T extends BasicEntity> C getDAO(Class<T> cls, DataSource ds, Object printer) throws Exception{
        return getDAO(cls, ds, null, printer);
    }
    
    public static GeneralDAO getDAO(DataSource ds) {
        GeneralDAO dao = new GeneralDAO(ds);
        return dao;
    }
    
    public static GeneralDAO getDAO(Connection conn) {
        GeneralDAO dao = new GeneralDAO(conn);
        return dao;
    }
    
    @SuppressWarnings("unchecked")
    public static <C extends AbstractDAO<T>, T extends BasicEntity> C getDAO(Class<T> cls, DataSource ds, Connection conn, Object printer) throws Exception{
        if (!JPACache.isCache){
            String className = cls.getName();
            int index = className.lastIndexOf(".");
            if (index != -1){
                String packagePath = className.substring(0, index);
                JPACache.cache(printer, packagePath);
            } else {
                System.out.println("Warning: domain objects are in default package");
                JPACache.cache(printer, "");
            }
        }
        BasicEntityManager.checkEntityAnnotation(cls);
        EntityListeners listener = cls.getAnnotation(EntityListeners.class);
        if (listener != null && listener.value().length > 0){
            Object obj = listener.value()[0].newInstance();
            if (obj instanceof AbstractDAO){
                AbstractDAO<?> adao = (AbstractDAO<?>) obj;
                EntityManager mgr = new EntityManager(ds, conn);
                adao.setEntityManager(mgr);
                if (!adao.getType().equals(cls)){
                    throw new RuntimeException(cls.getName() + " can not create DAO " + adao.getDAOType() + ", please check Entity annotation");
                }
                if (adao.getDAOType().isAssignableFrom(adao.getClass())){
                    return (C) adao.getClass().cast(adao);
                }
            }
        } else {
            throw new Exception("Please define daoClassName in " + cls.getName() + " Entity annotation!");
        }
        return null;
    }
    
    public static Connection getConnection(String driver, String user, String password, String url) {
        Connection conn;
        try {
            try {
                Class.forName(driver);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            conn = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return conn;
    }
    
}