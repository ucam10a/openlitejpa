package com.yung.jpa;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URLDecoder;
import java.util.Properties;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;

/**
 * Binding JDK Logger and SLF4J Logger
 * 
 * @author Yung-Long Li
 *
 */
public class AppLogger {

    private static final Level level = Level.INFO;
    private static boolean IS_SLF4J_BINDING_FAIL;
    
    private static Class<?> debugCls;
    private static String debugMethod;
    private static String mockConfig;
    private static String debugField;
    
    private String prefix;
    private java.util.logging.Logger jdkLogger;
    private org.slf4j.Logger slf4jLogger;
    private static String pattern;
    
    static {
        try {
            String name = org.slf4j.LoggerFactory.getILoggerFactory().getClass().getName();
            name = name.toLowerCase();
            if (name.contains("noploggerfactory")) {
                IS_SLF4J_BINDING_FAIL = true;
            } else {
                IS_SLF4J_BINDING_FAIL = false;
            }
        } catch (Exception e) {
            IS_SLF4J_BINDING_FAIL = true;
        }
        try {
            Properties prop = new Properties();
            prop.load(AppLogger.class.getClassLoader().getResourceAsStream("logging.properties"));
            pattern = prop.getProperty("java.util.logging.FileHandler.pattern");
            if (pattern != null && !"".equals(pattern)) {
                generateDir(new File(pattern).getAbsolutePath());
            }
        } catch (Exception e) {
        }
    }
    
    public static void setBindJDKLogging(boolean binding) {
        IS_SLF4J_BINDING_FAIL = binding;
        try {
            if (!IS_SLF4J_BINDING_FAIL) return;
            Properties prop = new Properties();
            prop.load(AppLogger.class.getClassLoader().getResourceAsStream("logging.properties"));
            pattern = prop.getProperty("java.util.logging.FileHandler.pattern");
            if (pattern != null && !"".equals(pattern)) {
                generateDir(new File(pattern).getAbsolutePath());
            }
        } catch (Exception e) {
        }
    }
    
    public static AppLogger getLogger(Class<?> cls) {
        AppLogger logger = new AppLogger();
        logger.jdkLogger = java.util.logging.Logger.getLogger(cls.getName());
        try {
            if (IS_SLF4J_BINDING_FAIL && pattern != null && !"".equals(pattern)) {
                FileHandler fileHandler = new FileHandler(pattern);
                fileHandler.setFormatter(new java.util.logging.SimpleFormatter());
                logger.jdkLogger.addHandler(fileHandler);
            }
        } catch (Exception e) {
        }
        logger.slf4jLogger = org.slf4j.LoggerFactory.getLogger(cls);
        logger.prefix = "{" + cls.getName() + "} ";
        return logger;
    }
    
    public static AppLogger getLogger(String clsName) {
        AppLogger logger = new AppLogger();
        logger.jdkLogger = java.util.logging.Logger.getLogger(clsName);
        try {
            if (IS_SLF4J_BINDING_FAIL && pattern != null && !"".equals(pattern)) {
                FileHandler fileHandler = new FileHandler(pattern);
                fileHandler.setFormatter(new java.util.logging.SimpleFormatter());
                logger.jdkLogger.addHandler(fileHandler);
            }
        } catch (Exception e) {
        }
        logger.slf4jLogger = org.slf4j.LoggerFactory.getLogger(clsName);
        logger.prefix = "{" + clsName + "} ";
        return logger;
    }
    
    private static File getFile(String filePath) {
        try {
            filePath = filePath.replace('\\', '/');
            File file = null;
            if (filePath.startsWith("file:")) {
                file = new File(new URI(filePath));
            } else {
                filePath = URLDecoder.decode(filePath, "UTF-8");
                file = new File(filePath);
            }
            return file;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    private static String getFileURI(String source_folder) {
        try {
            File temp = getFile(source_folder);
            return temp.toURI().toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    private static void generateDir(String filePath) {
        try {
            String[] dirs = getFileURI(filePath).split("/");
            int subtract = 0;
            if (dirs[dirs.length - 1].contains(".")) {
                subtract = 1;
            }
            String createDir = dirs[0];
            for (int i = 1; i < dirs.length - subtract; i++) {
                if (createDir.equals("")) {
                    createDir = dirs[i];
                } else {
                    createDir = createDir + "/" + dirs[i];
                }
                File fDir = getFile(createDir);
                if (!fDir.exists()) {
                    fDir.mkdirs();
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public void finest(String msg) {
        trace(msg);
    }
    
    public void trace(String msg) {
        msg = prefix + msg;
        if (IS_SLF4J_BINDING_FAIL) {
            jdkLogger.finest(msg);
        } else {
            slf4jLogger.trace(msg);
        }
    }
    
    public void trace(String msg, Throwable th) {
        msg = prefix + msg;
        if (IS_SLF4J_BINDING_FAIL) {
            jdkLogger.log(Level.FINEST, msg, th);
        } else {
            slf4jLogger.trace(msg, th);
        }
    }
    
    public void fine(String msg) {
        debug(msg);
    }
    
    public void debug(String msg) {
        msg = prefix + msg;
        if (IS_SLF4J_BINDING_FAIL) {
            jdkLogger.fine(msg);
        } else {
            slf4jLogger.debug(msg);
        }
    }
    
    public void debug(String msg, Throwable th) {
        msg = prefix + msg;
        if (IS_SLF4J_BINDING_FAIL) {
            jdkLogger.log(Level.FINE, msg, th);
        } else {
            slf4jLogger.debug(msg, th);
        }
    }
    
    public void info(String msg) {
        msg = prefix + msg;
        if (IS_SLF4J_BINDING_FAIL) {
            jdkLogger.info(msg);
        } else {
            slf4jLogger.info(msg);
        }
    }
    
    public void info(String msg, Throwable th) {
        msg = prefix + msg;
        if (IS_SLF4J_BINDING_FAIL) {
            jdkLogger.log(Level.INFO, msg, th);
        } else {
            slf4jLogger.info(msg, th);
        }
    }
    
    public void warning(String msg) {
        warn(msg);
    }
    
    public void warn(String msg) {
        msg = prefix + msg;
        if (IS_SLF4J_BINDING_FAIL) {
            jdkLogger.warning(msg);
        } else {
            slf4jLogger.warn(msg);
        }
    }
    
    public void warn(String msg, Throwable th) {
        msg = prefix + msg;
        if (IS_SLF4J_BINDING_FAIL) {
            jdkLogger.log(Level.WARNING, msg, th);
        } else {
            slf4jLogger.warn(msg, th);
        }
    }
    
    public void error(String msg) {
        msg = prefix + msg;
        if (IS_SLF4J_BINDING_FAIL) {
            jdkLogger.severe(msg);
        } else {
            slf4jLogger.error(msg);
        }
    }
    
    public void error(String msg, Throwable th) {
        msg = prefix + msg;
        if (IS_SLF4J_BINDING_FAIL) {
            jdkLogger.log(Level.SEVERE, msg, th);
        } else {
            slf4jLogger.error(msg, th);
        }
    }
    
    public void severe(String msg) {
        error(msg);
    }
    
    public void log(Level level, String msg) {
        if (level == Level.INFO) {
            info(msg);
        } else if (level == Level.FINE) {
            debug(msg);
        } else if (level == Level.FINER) {
            trace(msg);
        } else if (level == Level.FINEST) {
            trace(msg);
        } else if (level == Level.WARNING) {
            warn(msg);
        } else if (level == Level.SEVERE) {
            error(msg);
        }
    }
    
    public void log(Level level, String msg, Throwable th) {
        if (level == Level.INFO) {
            info(msg, th);
        } else if (level == Level.FINE) {
            debug(msg, th);
        } else if (level == Level.FINER) {
            trace(msg, th);
        } else if (level == Level.FINEST) {
            trace(msg, th);
        } else if (level == Level.WARNING) {
            warn(msg, th);
        } else if (level == Level.SEVERE) {
            error(msg, th);
        }
    }
    
    public static void setLogLevel(java.util.logging.Logger logger){
        logger.setLevel(level);
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(level);
        logger.addHandler(consoleHandler);
    }
    
    public static Class<?> getDebugCls() {
        if (debugCls != null) {
            return debugCls;
        }
        java.util.logging.Logger jdkLogger = java.util.logging.Logger.getLogger(AppLogger.class.getName());
        org.slf4j.Logger slf4jLogger = org.slf4j.LoggerFactory.getLogger(AppLogger.class);
        try {
            ClassLoader loader = AppLogger.class.getClassLoader();
            Properties prop = new Properties();
            InputStream stream = loader.getResourceAsStream("debug.properties");
            if (stream == null) return null; 
            prop.load(loader.getResourceAsStream("debug.properties"));
            String debugClsName = prop.getProperty("debugCls");
            if (debugCls == null) {
                return null;
            }
            debugCls = Class.forName(debugClsName);
            jdkLogger.info("debugCls: " + debugCls.getName());
            slf4jLogger.info("debugCls: " + debugCls.getName());
            return debugCls;
        } catch(Exception e) {
            jdkLogger.log(Level.FINE, e.toString(), e);
            slf4jLogger.trace(e.toString(), e);
        }
        return null;
    }

    public static String getDebugMethod() {
        if (debugMethod != null) {
            return debugMethod;
        }
        java.util.logging.Logger jdkLogger = java.util.logging.Logger.getLogger(AppLogger.class.getName());
        org.slf4j.Logger slf4jLogger = org.slf4j.LoggerFactory.getLogger(AppLogger.class);
        try {
            ClassLoader loader = AppLogger.class.getClassLoader();
            Properties prop = new Properties();
            InputStream stream = loader.getResourceAsStream("debug.properties");
            if (stream == null) return ""; 
            prop.load(loader.getResourceAsStream("debug.properties"));
            String debugMethodName = prop.getProperty("debugMethod");
            if (debugMethodName == null || debugMethodName.equals("")) {
                return "";
            } else {
                debugMethod = debugMethodName;
            }
            jdkLogger.info("debugMethod: " + debugMethod);
            slf4jLogger.info("debugMethod: " + debugMethod);
            return debugMethod;
        } catch(Exception e) {
            jdkLogger.log(Level.FINE, e.toString(), e);
            slf4jLogger.trace(e.toString(), e);
        }
        return "";
    }

    public static String getMockConfig() {
        if (mockConfig != null) {
            return mockConfig;
        }
        java.util.logging.Logger jdkLogger = java.util.logging.Logger.getLogger(AppLogger.class.getName());
        org.slf4j.Logger slf4jLogger = org.slf4j.LoggerFactory.getLogger(AppLogger.class);
        try {
            ClassLoader loader = AppLogger.class.getClassLoader();
            Properties prop = new Properties();
            InputStream stream = loader.getResourceAsStream("debug.properties");
            if (stream == null) return ""; 
            prop.load(loader.getResourceAsStream("debug.properties"));
            String mockConfigName = prop.getProperty("mockConfig");
            if (mockConfigName == null || mockConfigName.equals("")) {
                return "";
            } else {
                mockConfig = mockConfigName;
            }
            jdkLogger.info("mockConfig: " + mockConfig);
            slf4jLogger.info("mockConfig: " + mockConfig);
            return mockConfig;
        } catch(Exception e) {
            jdkLogger.log(Level.FINE, e.toString(), e);
            slf4jLogger.trace(e.toString(), e);
        }
        return "";
    }

    public static String getDebugField() {
        if (debugField != null) {
            return debugField;
        }
        java.util.logging.Logger jdkLogger = java.util.logging.Logger.getLogger(AppLogger.class.getName());
        org.slf4j.Logger slf4jLogger = org.slf4j.LoggerFactory.getLogger(AppLogger.class);
        try {
            ClassLoader loader = AppLogger.class.getClassLoader();
            Properties prop = new Properties();
            InputStream stream = loader.getResourceAsStream("debug.properties");
            if (stream == null) return ""; 
            prop.load(loader.getResourceAsStream("debug.properties"));
            String debugFieldName = prop.getProperty("debugField");
            if (debugField == null || debugField.equals("")) {
                return "";
            } else {
                debugField = debugFieldName;
            }
            jdkLogger.info("debugField: " + debugField);
            slf4jLogger.info("debugField: " + debugField);
            return debugField;
        } catch(Exception e) {
            jdkLogger.log(Level.FINE, e.toString(), e);
            slf4jLogger.trace(e.toString(), e);
        }
        return "";
    }
    
}