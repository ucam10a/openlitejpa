package com.yung.jpa;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Map;

public class TypeTool {

    private static NumberConverter nConverter = new NumberConverter();
    private static TimeConverter tConverter = new TimeConverter();
    
    public static String getString(Map<String, Object> rawMap, String key) {
        Object val = rawMap.get(key);
        return (String) val;
    }
    
    public static Integer getInteger(Map<String, Object> rawMap, String key) {
        Object val = rawMap.get(key);
        if (val == null) return null;
        Integer ret = (Integer) nConverter.Convert(val, Integer.class);
        return ret;
    }
    
    public static BigDecimal getBigDecimal(Map<String, Object> rawMap, String key) {
        Object val = rawMap.get(key);
        if (val == null) return null;
        BigDecimal ret = (BigDecimal) nConverter.Convert(val, BigDecimal.class);
        return ret;
    }
    
    public static Timestamp getTimestamp(Map<String, Object> rawMap, String key) {
        Object val = rawMap.get(key);
        if (val == null) return null;
        Timestamp ret = (Timestamp) tConverter.Convert(val, Timestamp.class);
        return ret;
    }
    
    public static java.util.Date getDate(Map<String, Object> rawMap, String key) {
        Object val = rawMap.get(key);
        if (val == null) return null;
        java.util.Date ret = (java.util.Date) tConverter.Convert(val, java.util.Date.class);
        return ret;
    }
    
}