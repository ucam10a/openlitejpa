package com.yung.jpa.tool;

import java.util.List;

public class JPAEntityOBJ extends EntityOBJ {

    private String schema;
    
    public JPAEntityOBJ(){
    }
    
    public JPAEntityOBJ(String schema, String className, String tableName, List<Attribute> attributes) {
        this.schema = schema;
        this.className = className;
        this.tableName = tableName;
        this.attributes = attributes;
    }
    
    public String generateCode(){
        bud = new StringBuilder();
        // Entity
        append("@Entity");
        append("@Table(name=\"" + tableName + "\", schema=\"" + schema + "\")");
        append("@IdClass(" + upperFirstChar(className) + "Id.class)");
        append("public class " + upperFirstChar(className) + " implements Serializable {");
        for (Attribute attr : attributes) {
            append("");
            if (attr.isKey()) {
                append("    @Id");
            }
            append("    @Column(name=\"" + attr.getAttributeName() + "\")");
            append("    private " + attr.getType() + " " + attr.getObjectName() + ";");
        }
        for (Attribute attr : attributes) {
            append("");
            append("    public void set" + upperFirstChar(attr.getObjectName()) + "(" + attr.getType() + " " + attr.getObjectName() + ") {");
            append("        this." + attr.getObjectName() + " = " + attr.getObjectName() + ";");
            append("    }");
            append("");
            append("    public " + attr.getType() + " get" + upperFirstChar(attr.getObjectName()) + "() {");
            append("        return " + attr.getObjectName() + ";");
            append("    }");
        }
        append("");
        append("    public " + upperFirstChar(className) + "Id getId() {");
        append("        " + upperFirstChar(className) + "Id id = new " + upperFirstChar(className) + "Id();");
        for (Attribute attr : attributes) {
            if (attr.isKey()) {
                append("        id.set" + upperFirstChar(attr.getObjectName()) + "(this." + attr.getObjectName() + ");");
            }
        }
        append("        return id;");
        append("    }");
        append("}");
        append("\n\n");
        append("public class " + upperFirstChar(className) + "Id implements Serializable {");
        for (Attribute attr : attributes) {
            if (attr.isKey()) {
                append("");
                append("    private " + attr.getType() + " " + attr.getObjectName() + ";");
            }
        }
        for (Attribute attr : attributes) {
            if (attr.isKey()) {
                append("");
                append("    public void set" + upperFirstChar(attr.getObjectName()) + "(" + attr.getType() + " " + attr.getObjectName() + ") {");
                append("        this." + attr.getObjectName() + " = " + attr.getObjectName() + ";");
                append("    }");
                append("");
                append("    public " + attr.getType() + " get" + upperFirstChar(attr.getObjectName()) + "() {");
                append("        return " + attr.getObjectName() + ";");
                append("    }");
            }
        }
        append("}");
        return bud.toString();
    }
    
    public static void main(String[] args) throws Exception {
        
        
        
    }
}