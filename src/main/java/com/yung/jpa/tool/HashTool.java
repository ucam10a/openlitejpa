package com.yung.jpa.tool;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Date;

import javax.persistence.EmbeddedId;

import com.yung.jpa.HashIndex;
import com.yung.jpa.NumberConverter;
import com.yung.jpa.PlainObjectOperator;
import com.yung.jpa.TimeConverter;

public class HashTool {

    public static String getHash(Object obj) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        StringBuilder sb = new StringBuilder();
        getHash(obj, sb);
        return getMD5(sb.toString());
    }
    
    private static void getHash(Object obj, StringBuilder sb) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        if (obj == null) {
            return;
        }
        NumberConverter nConverter = new NumberConverter();
        TimeConverter tConverter = new TimeConverter();
        for (Field f : obj.getClass().getDeclaredFields()) {
            EmbeddedId e = f.getAnnotation(EmbeddedId.class);
            HashIndex h = f.getAnnotation(HashIndex.class);
            if (e != null) {
                Object id = PlainObjectOperator.runGetter(f, obj);
                getHash(id, sb);
            } else {
                if (h != null) {
                    Object val = PlainObjectOperator.runGetter(f, obj);
                    if (val != null) {
                        if (val.getClass() == String.class) {
                            sb.append(val.toString());
                        } else if (nConverter.isNumber(val.getClass())) {
                            BigDecimal num = (BigDecimal) nConverter.Convert(val, BigDecimal.class);
                            num = num.setScale(h.decimalScale(), h.mode());
                            sb.append(num.toPlainString());
                        } else if (tConverter.isTime(val.getClass())) {
                            java.util.Date dt = (Date) tConverter.Convert(val, java.util.Date.class);
                            sb.append(dt.getTime() + "");
                        } else {
                            throw new RuntimeException("Not support type: ");
                        }
                    }
                }
            }
        }
    }
 
    private static String getMD5(String input) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(input.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1, digest);
            String hashtext = bigInt.toString(16);
            // Now we need to zero pad it if you actually want the full 32 chars.
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
}
