package com.yung.jpa.tool;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.yung.jpa.tool.Attribute.Type;

public class EntityOBJ {

    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
    
    protected String className;
    
    protected String tableName;
    
    protected List<Attribute> attributes = new ArrayList<Attribute>();
    
    protected StringBuilder bud = new StringBuilder();
    
    public EntityOBJ(){
    }
    
    public static List<Attribute> getAttributes(Connection conn, String schema, String table) throws SQLException {
        List<Attribute> columns = new ArrayList<Attribute>();
        DatabaseMetaData dbmd = conn.getMetaData();
        ResultSet rs = dbmd.getPrimaryKeys(null, null, table.toUpperCase());
        Set<String> keyColumns = new HashSet<String>();
        while(rs.next()){
            keyColumns.add(rs.getString("COLUMN_NAME").toUpperCase());
        }
        Statement st = conn.createStatement();
        String sql = null;
        if (schema != null && !schema.equals("")) {
            sql = "SELECT * FROM " + schema + "." + table;
        } else {
            sql = "SELECT * FROM " + table;
        }
        rs = st.executeQuery(sql);
        ResultSetMetaData rsMetaData = rs.getMetaData();
        int numberOfColumns = rsMetaData.getColumnCount();
        for (int i = 1; i <= numberOfColumns; i++) {
            rsMetaData.getColumnTypeName(i);
            String columnName = rsMetaData.getColumnName(i);
            String type = rsMetaData.getColumnTypeName(i);
            boolean isKey = false;
            if (keyColumns.contains(columnName.toUpperCase())) {
                isKey = true;
            }
            columns.add(new Attribute(getType(type), rename(columnName), columnName, isKey));
        }
        return columns;
    }
    
    public EntityOBJ(String className, String tableName, Attribute... attributes){
        this.className = className;
        this.tableName = tableName;
        for (Attribute attr : attributes) {
            this.attributes.add(attr);
        }
    }
    
    public EntityOBJ(String className, String tableName, List<Attribute> attributes){
        this.className = className;
        this.tableName = tableName;
        this.attributes = attributes;
    }
    
    public void append(String code) {
        bud.append(code + LINE_SEPARATOR);
    }
    
    public String generateCode(){
        bud = new StringBuilder();
        // Entity
        append("@Entity(name=\"" + tableName + "\")");
        append("@EntityListeners(value={" + className + "DAO.class})");
        append("public class " + className + " extends BasicEntity implements Serializable {");
        for (Attribute attr : attributes) {
            append("");
            append("    @Column(name=\"" + attr.getAttributeName() + "\")");
            if (attr.isKey()) {
                append("    @Id");
            }
            append("    private " + attr.getType() + " " + attr.getObjectName() + ";");
        }
        for (Attribute attr : attributes) {
            append("");
            append("    public void set" + upperFirstChar(attr.getObjectName()) + "(" + attr.getType() + " " + attr.getObjectName() + ") {");
            append("        this." + attr.getObjectName() + " = " + attr.getObjectName() + ";");
            append("    }");
            append("");
            append("    public " + attr.getType() + " get" + upperFirstChar(attr.getObjectName()) + "() {");
            append("        return " + attr.getObjectName() + ";");
            append("    }");
        }
        append("}");
        // DAO
        append("");
        append("public class " + className + "DAO extends AbstractDAO<" + className + "> {");
        append("}");
        return bud.toString();
    }

    protected static String upperFirstChar(String objectName) {
        String name = objectName.trim();
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < name.length(); i++) {
            String character = name.charAt(i) + "";
            if (i == 0) {
                character = character.toUpperCase();
            }
            buf.append(character);
        }
        return buf.toString();
    }
    
    private static Type getType(String type) {
        // oracle
        if (type.toLowerCase().contains("string")) {
            return Type.String;
        } else if (type.toLowerCase().contains("integer")) {
            return Type.Integer;
        } else if (type.toLowerCase().contains("int")) {
            return Type.Integer;
        } else if (type.toLowerCase().contains("timestamp")) {
            return Type.Timestamp;
        } else if (type.toLowerCase().contains("boolean")) {
            return Type.Boolean;
        } else if (type.toLowerCase().contains("date")) {
            return Type.Timestamp;
        } else if (type.toLowerCase().contains("long")) {
            return Type.Long;
        } else if (type.toLowerCase().contains("double")) {
            return Type.Double;
        } else if (type.toLowerCase().contains("biginteger")) {
            return Type.BigInteger;
        } else if (type.toLowerCase().contains("bigdecimal")) {
            return Type.BigDecimal;
        } else if (type.toLowerCase().contains("varchar2")) {
            return Type.String;
        } else if (type.toLowerCase().contains("clob")) {
            return Type.Clob;
        } else if (type.toLowerCase().contains("number")) {
            return Type.BigDecimal;
        } else if (type.toLowerCase().contains("decimal")) {
            return Type.BigDecimal;
        } else if (type.toLowerCase().contains("varchar")) {
            return Type.String;
        } else if (type.toLowerCase().contains("blob")) {
            return Type.Blob;
        } else {
            throw new RuntimeException("unknown type: " + type);
        }
    }

    private static String rename(String columnName) {
        if (columnName.contains("_")) {
            String[] tokens = columnName.split("_");
            StringBuilder bud = new StringBuilder();
            for (int i = 0; i < tokens.length; i++) {
                String token = tokens[i].trim().toLowerCase();
                if (i != 0) {
                    bud.append(upperFirstChar(token));
                } else {
                    bud.append(token);
                }
            }
            return bud.toString();
        } else {
            return columnName.toLowerCase();
        }
    }
    
    public static String createClassName(String tableName) {
        String className = rename(tableName);
        return upperFirstChar(className);
    }
    
    public static void main(String[] args) throws Exception {
        
        EntityOBJ entity = new EntityOBJ(
                "UserOBJ", "USERS"
                , new Attribute(Type.String, "id", "ID", true)
                , new Attribute(Type.String, "name", "NAME", false)
                , new Attribute(Type.Timestamp, "createDate", "CREATE_DT", false)
                , new Attribute(Type.String, "createUser", "CREATE_USER", false)
                );
        
        String code = entity.generateCode();
        System.out.println(code);
        
        
    }

}