package com.yung.jpa;

import java.lang.reflect.Type;
import java.sql.Timestamp;

public class TimeConverter extends ObjectConverter {

    public boolean isTime(Type type) {
        Class<?> cls = getClass(type);
        return isTime(cls);
    }
    
    public boolean isTime(Class<?> cls) {
        if (cls == java.util.Date.class) {
            return true;
        } else if (cls == java.sql.Date.class) {
            return true;
        } else if (cls == Timestamp.class) {
            return true;
        }
        return false;
    }
    
    @Override
    public Object Convert(Object obj, Class<?> cls) {
        long time = 0;
        if (obj.getClass() == java.sql.Date.class) {
            java.sql.Date dt = (java.sql.Date) obj;
            time = dt.getTime();
        } else if (obj.getClass() == java.util.Date.class) {
            java.util.Date dt = (java.util.Date) obj;
            time = dt.getTime();
        } else if (obj.getClass() == Timestamp.class) {
            Timestamp dt = (Timestamp) obj;
            time = dt.getTime();
        } else {
            throw new RuntimeException("unsupport obj: " + obj.getClass().getName());
        }
        if (cls.isAssignableFrom(java.sql.Date.class)) {
            return new java.sql.Date(time);
        } else if (cls.isAssignableFrom(java.util.Date.class)) {
            return new java.util.Date(time);
        } else if (cls.isAssignableFrom(Timestamp.class)) {
            return new Timestamp(time);
        } else {
            throw new RuntimeException("unsupport return class: " + cls.getName());
        }
    }

    
    
}
