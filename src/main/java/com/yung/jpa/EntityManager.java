package com.yung.jpa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.*;
import javax.sql.DataSource;

public class EntityManager extends BasicEntityManager {
    
    private static final AppLogger logger = AppLogger.getLogger(EntityManager.class);
    private static boolean DEBUG = false;
    
    private static String RMDB = "";
    
    private static DebugPrinterTool printer;
    
    public EntityManager(DataSource dataSource, Connection conn){
        this.setDataSource(dataSource);
        this.setConnection(conn);
        if ("".equals(RMDB) && conn != null) {
            try {
                String driver = conn.getClass().getName();
                if (driver.contains("hsqldb")) {
                    RMDB = "hsqldb";
                } else if (driver.contains("mysql")) {
                    RMDB = "mysql";
                } else if (driver.contains("postgresql")) {
                    RMDB = "postgresql";
                } else if (driver.contains("sqlserver")) {
                    RMDB = "sqlserver";
                } else if (driver.contains("oracle")) {
                    RMDB = "oracle";
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
    
    public static String processSQL(String SQL) {
        return SQL;
    }
    
    public static void setPrinter(Object tool) {
        if (tool == null || printer != null) {
            return;
        }
        EntityManager.printer = new DebugPrinterTool(tool);
    }
    
    public static void enableDebug() {
        DEBUG = true;
    }
    
    public static void disableDebug() {
        DEBUG = false;
    }
    
    public static void printSql(String sql) {
        if (printer == null) {
            return;
        }
        printer.printObjectParam("sql", sql);
    }
    
    public static void printParam(Object... objs) {
        if (printer == null) {
            return;
        }
        printer.printObjectParam("param", objs);
    }
    
    public String getDriverName() throws SQLException {
        return connection.getMetaData().getDriverName();
    }
    
    public static void printObject(Object obj) {
        if (printer == null) {
            return;
        }
        printer.printObjectParam("obj", obj);
    }
    
    public <T extends BasicEntity> void insert(T obj) throws Exception {
        checkEntityAnnotation(obj.getClass());
        PreparedStatement ps = null;
        try {
            prepareConnection();
            String sql = createInsertSql(obj.getClass());
            sql = processSQL(sql);
            if (DEBUG) {
                System.out.println(sql);
                printSql(sql);
                printObject(obj);
            }
            ps = connection.prepareStatement(sql);
            setParameters(ps, obj);
            ps.executeUpdate();
            updatePKCache(obj);
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public <T extends BasicEntity> void insertList(List<T> objList) throws Exception {
        if (objList == null || objList.size() == 0) {
            return;
        }
        checkEntityAnnotation(objList.get(0).getClass());
        PreparedStatement ps = null;
        try {
            prepareConnection();
            String sql = createInsertSql(objList.get(0).getClass());
            sql = processSQL(sql);
            ps = connection.prepareStatement(sql);
            for (T obj : objList) {
                setParameters(ps, obj);
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public <T extends BasicEntity> void update(T obj) throws Exception {
        checkEntityAnnotation(obj.getClass());
        PreparedStatement ps = null;
        try {
            prepareConnection();
            String sql = createUpdateSql(obj.getClass());
            sql = processSQL(sql);
            if (DEBUG) {
                System.out.println(sql);
                printSql(sql);
                printObject(obj);
            }
            ps = connection.prepareStatement(sql);
            int index = setParameters(ps, obj);
            setPrimaryKeyParameters(index, ps, obj);
            ps.executeUpdate();
            updatePKCache(obj);
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public <T extends BasicEntity> void update(T obj, String... fields) throws Exception {
        checkEntityAnnotation(obj.getClass());
        PreparedStatement ps = null;
        try {
            prepareConnection();
            String sql = createUpdateSql(obj.getClass(), fields);
            sql = processSQL(sql);
            if (DEBUG) {
                System.out.println(sql);
                printSql(sql);
                printObject(obj);
            }
            ps = connection.prepareStatement(sql);
            int index = setParameters(ps, obj, fields);
            setPrimaryKeyParameters(index, ps, obj);
            ps.executeUpdate();
            updatePKCache(obj);
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public <T extends BasicEntity> void delete(T obj) throws Exception {
        checkEntityAnnotation(obj.getClass());
        PreparedStatement ps = null;
        try {
            prepareConnection();
            String sql = createDeleteSql(obj.getClass());
            sql = processSQL(sql);
            if (DEBUG) {
                System.out.println(sql);
                printSql(sql);
                printObject(obj);
            }
            ps = connection.prepareStatement(sql);
            @SuppressWarnings("unchecked")
            T key = (T) construct(obj, obj.getClass());
            updatePKCache(key);
            setPrimaryKeyParameters(ps, key);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public <T extends BasicEntity> void deleteList(List<T> objList) throws Exception {
        if (objList == null || objList.size() == 0) {
            return;
        }
        checkEntityAnnotation(objList.get(0).getClass());
        PreparedStatement ps = null;
        try {
            prepareConnection();
            String sql = createDeleteSql(objList.get(0).getClass());
            sql = processSQL(sql);
            ps = connection.prepareStatement(sql);
            for (T obj : objList) {
                setPrimaryKeyParameters(ps, obj);
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }

    public <T extends BasicEntity> T findByPrimaryKey(Class<T> objClass, T obj) throws Exception {
        checkEntityAnnotation(objClass);
        Entity en = objClass.getAnnotation(Entity.class);
        EntityClass ec = getEntityClass(en.name());
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            prepareConnection();
            String sql = createSelectSql(en.name(), true);
            sql = processSQL(sql);
            if (DEBUG) {
                System.out.println(sql);
                printSql(sql);
                printObject(obj);
            }
            ps = connection.prepareStatement(sql);
            setPrimaryKeyParameters(ps, obj);
            rs = ps.executeQuery();
            T result = null;
            if (rs.next()) {
                result = constructObject(objClass, ec, rs);
            }
            return result;
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public <T extends BasicEntity> List<T> listAll(Class<T> objClass) throws Exception {
        checkEntityAnnotation(objClass);
        Entity en = objClass.getAnnotation(Entity.class);
        EntityClass ec = getEntityClass(en.name());
        List<T> results = new ArrayList<T>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            prepareConnection();
            String sql = createSelectSql(ec.getTable(), false);
            sql = processSQL(sql);
            if (DEBUG) {
                System.out.println(sql);
                printSql(sql);
            }
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            T result = null;
            while (rs.next()) {
                result = constructObject(objClass, ec, rs);
                results.add(result);
            }
            return results;
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public <T extends BasicEntity> List<T> find(Class<T> objClass, String whereSql, Object... params) throws Exception {
        checkEntityAnnotation(objClass);
        Entity en = objClass.getAnnotation(Entity.class);
        EntityClass ec = getEntityClass(en.name());
        List<T> results = new ArrayList<T>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            prepareConnection();
            String sql = createSelectStatement(ec.getTable());
            whereSql = compileWhereSql(objClass, whereSql);
            sql = sql + " " + whereSql;
            sql = processSQL(sql);
            if (DEBUG) {
                System.out.println(sql);
                printSql(sql);
                printParam(params);
            }
            ps = connection.prepareStatement(sql);
            int idx = 1;
            for (Object param : params) {
                setParam(ps, idx, param);
                idx++;
            }
            rs = ps.executeQuery();
            T result = null;
            while (rs.next()) {
                result = constructObject(objClass, ec, rs);
                results.add(result);
            }
            return results;
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    private <T extends BasicEntity> String compileWhereSql(Class<T> cls, String whereSql) {
        for (FieldClass f : getFieldClass(cls.getName())){
            String name = f.f.getName();
            if (f.c != null) {
                String columnName = f.c.name();
                int start = 0;
                int idx = whereSql.indexOf(name, start);
                while (idx > 0) {
                    if (whereSql.charAt(idx + name.length()) == ' ') {
                        whereSql = whereSql.substring(0, idx) + columnName + whereSql.substring(idx + name.length());
                        start = idx + columnName.length();
                    } else {
                        start = whereSql.indexOf(" ", idx);
                    }
                    idx = whereSql.indexOf(name, start);
                }
            }
        }
        return whereSql;
    }

    public <T extends BasicEntity> List<T> find(Class<T> objClass, T obj) throws Exception {
        checkEntityAnnotation(objClass);
        Entity en = objClass.getAnnotation(Entity.class);
        EntityClass ec = getEntityClass(en.name());
        List<T> results = new ArrayList<T>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            prepareConnection();
            List<String> tables = new ArrayList<String>();
            findAllTables(obj, ec, tables);
            String sql = createSelectSql(obj, ec, tables);
            sql = processSQL(sql);
            if (DEBUG) {
                System.out.println(sql);
                printSql(sql);
                printObject(obj);
            }
            ps = connection.prepareStatement(sql);
            setParameters(ps, obj, ec, tables);
            rs = ps.executeQuery();
            while (rs.next()) {
                T result = constructObject(objClass, ec, rs);
                if (result != null) results.add(result);
            }
            return results;
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public void flush(){
        try {
            if (connection != null){
                connection.commit();
                connection.close();
            }
        } catch (SQLException e) {
            logger.warn(e.toString(), e);
        }
    }
    
    public Connection prepareConnection() throws SQLException{
        if (connection == null) {
            setConnection(getDataSource().getConnection());
            connection = getConnection();
            connection.setAutoCommit(false);
        }
        return connection;
    }

    public <T> List<T> findBySql(Class<T> cls, String sql, Object[] params) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            List<T> results = new ArrayList<T>();
            prepareConnection();
            sql = processSQL(sql);
            if (DEBUG) {
                System.out.println(sql);
                printSql(sql);
                printParam(params);
            }
            ps = connection.prepareStatement(sql);
            int idx = 1;
            for (Object param : params) {
                setParam(ps, idx, param);
                idx++;
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                T obj = setup(cls, rs);
                results.add(obj);
            }
            return results;
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }

    public List<Map<String, Object>> findBySql(String sql, Object[] params) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();
            prepareConnection();
            sql = processSQL(sql);
            if (DEBUG) {
                System.out.println(sql);
                printSql(sql);
                printParam(params);
            }
            ps = connection.prepareStatement(sql);
            int idx = 1;
            for (Object param : params) {
                setParam(ps, idx, param);
                idx++;
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                Map<String, Object> map = getMap(rs);
                results.add(map);
            }
            return results;
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public void insertBySql(String sql, Object[] params) throws SQLException {
        PreparedStatement ps = null;
        try {
            prepareConnection();
            sql = processSQL(sql);
            if (DEBUG) {
                System.out.println(sql);
                printSql(sql);
                printParam(params);
            }
            ps = connection.prepareStatement(sql);
            int idx = 1;
            for (Object param : params) {
                setParam(ps, idx, param);
                idx++;
            }
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public void updateBySql(String sql, Object[] params) throws SQLException {
        PreparedStatement ps = null;
        try {
            prepareConnection();
            sql = processSQL(sql);
            if (DEBUG) {
                System.out.println(sql);
                printSql(sql);
                printParam(params);
            }
            ps = connection.prepareStatement(sql);
            int idx = 1;
            for (Object param : params) {
                setParam(ps, idx, param);
                idx++;
            }
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }

    public void deleteBySql(String sql, Object[] params) throws SQLException {
        PreparedStatement ps = null;
        try {
            prepareConnection();
            sql = processSQL(sql);
            if (DEBUG) {
                System.out.println(sql);
                printSql(sql);
                printParam(params);
            }
            ps = connection.prepareStatement(sql);
            int idx = 1;
            for (Object param : params) {
                setParam(ps, idx, param);
                idx++;
            }
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
}