package com.yung.jpa;

import java.util.HashMap;
import java.util.Map;

public class EntityClass {

    private String table;
    
    private String className;
    
    private Class<?> cls;
    
    private String daoClassName;
    
    private Class<?> daoCls;
    
    private String[] columns;
    
    private String[] pkColumns;
    
    private Map<String, FieldClass> fMap = new HashMap<String, FieldClass>();

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) throws ClassNotFoundException {
        this.className = className;
        this.cls = Class.forName(className);
    }

    public Class<?> getCls() {
        return cls;
    }

    public String getDaoClassName() {
        return daoClassName;
    }

    public void setDaoClassName(String daoClassName) throws ClassNotFoundException {
        this.daoClassName = daoClassName;
        this.daoCls = Class.forName(daoClassName);
    }

    public Class<?> getDaoCls() {
        return daoCls;
    }

    public String[] getColumns() {
        return columns;
    }

    public void setColumns(String[] columns) {
        this.columns = columns;
    }

    public String[] getPkColumns() {
        return pkColumns;
    }

    public void setPkColumns(String[] pkColumns) {
        this.pkColumns = pkColumns;
    }
    
    public void setColumnFieldClass(String columnName, FieldClass f){
        fMap.put(columnName, f);
    }

    public FieldClass getColumnFieldClass(String columnName){
        if (fMap.get(columnName) == null){
            throw new RuntimeException("table:" + table + " column:" + columnName + " doesn't exist !");
        }
        return fMap.get(columnName);
    }
    
    
}