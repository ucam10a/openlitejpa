package com.yung.jpa;

import java.util.ArrayList;
import java.util.List;

class ClassVisitor implements Visitor<VisitedClass> {
    
    List<VisitedClass> visited = new ArrayList<VisitedClass>();
    
    public void visit(VisitedClass item) {
        visited.add(item);
    }

    public boolean wasVisited(VisitedClass item) {
        if (visited.contains(item)) return true;
        return false;
    }
    
    public ClassVisitor clone(){
        ClassVisitor newVisitor = new ClassVisitor();
        newVisitor.visited.addAll(visited);
        return newVisitor;
    }
    
    @Override
    public String toString(){
        String result = "";
        for (VisitedClass cls : visited){
            result = result + cls.toString() + "=>";
        }
        if (!result.equals("")) result = result.substring(0 , result.length() - 2);
        return result;
    }
    
}
