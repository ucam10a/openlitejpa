package com.yung.jpa;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class SQLSanitizer {
    
    public static Object[] checkSQL(String format, Object[] inputs) {
        int length = 0;
        if (inputs != null) {
            length = inputs.length;
        }
        Object[] result = new Object[length];
        int counter = 0;
        int idx = 0;
        int idxStart = format.indexOf('{', idx) + 1;
        int idxEnd = format.indexOf('}', idx);
        while (idxEnd >= 0) {
            counter = Integer.valueOf(format.substring(idxStart, idxEnd));
            if (result[counter] == null) {
                char c = format.charAt(idxStart - 2);
                char c1 = format.charAt(idxStart - 3);
                if (c == '\'') {
                    // input value should be string then escape string
                    Object val = inputs[counter];
                    if (val != null) {
                        String str = val.toString().replace('\'', '`');
                        result[counter] = str;
                    } else {
                        result[counter] = inputs[counter];
                    }
                } else if (c == '%' && c1 == '\'') {
                    // input value should be string then escape string
                    Object val = inputs[counter];
                    if (val != null) {
                        String str = val.toString().replace('\'', '"');
                        result[counter] = str;
                    } else {
                        result[counter] = inputs[counter];
                    }
                } else {
                    // not string should be number
                    Object val = inputs[counter];
                    if (val != null && val.getClass() == String.class) {
                        if ("NULL".equalsIgnoreCase(val.toString())) {
                            result[counter] = null;
                        } else {
                            // pass value is string and it should be number string
                            try {
                                BigDecimal num = new BigDecimal(val.toString());
                                result[counter] = num;
                            } catch (Exception e) {
                                throw new RuntimeException("number[" + val.toString() + "] error!", e);
                            }
                        }
                    } else {
                        result[counter] = inputs[counter];
                    }
                }
            }
            idx = idxEnd + 1;
            idxStart = format.indexOf('{', idx) + 1;
            idxEnd = format.indexOf('}', idx);
        }
        return result;
    }
    
    public static Map<String, Object> checkSQL(String format, Map<String, Object> inputMap) {
        Map<String, Object> result = new HashMap<String, Object>();
        int idx = 0;
        int idxStart = format.indexOf('{', idx) + 1;
        int idxEnd = format.indexOf('}', idx);
        while (idxEnd >= 0) {
            String key = format.substring(idxStart, idxEnd);
            if (result.get(key) == null) {
                char c = format.charAt(idxStart - 2);
                char c1 = format.charAt(idxStart - 3);
                if (c == '\'') {
                    // input value should be string then escape string
                    Object val = inputMap.get(key);
                    if (val != null) {
                        String str = val.toString().replace('\'', '"');
                        result.put(key, str);
                    } else {
                        result.put(key, val);
                    }
                } else if (c == '%' && c1 == '\'') {
                    // input value should be string then escape string
                    Object val = inputMap.get(key);
                    if (val != null) {
                        String str = val.toString().replace('\'', '"');
                        result.put(key, str);
                    } else {
                        result.put(key, val);
                    }
                } else {
                    // not string should be number
                    Object val = inputMap.get(key);
                    if (val != null && val.getClass() == String.class) {
                        // pass value is string and it should be number string
                        try {
                            BigDecimal num = new BigDecimal(val.toString());
                            result.put(key, num);
                        } catch (Exception e) {
                            throw new RuntimeException("number[" + val.toString() + "] error!", e);
                        }
                    } else {
                        result.put(key, val);
                    }
                }
            }
            idx = idxEnd + 1;
            idxStart = format.indexOf('{', idx) + 1;
            idxEnd = format.indexOf('}', idx);
        }
        return result;
    }
    
    public static String formatSql(String sql, Map<String, Object> inputMap) {
        Map<String, Object> result = checkSQL(sql, inputMap);
        for (String key : result.keySet()) {
            Object value = result.get(key);
            if (value == null) {
                value = "null";
            }
            sql = replaceAll(sql, "{" + key + "}", value.toString());
        }
        return sql;
    }
    
    private static String replaceAll(String originalText, String key, String replaceValue) {
        if (originalText == null || originalText.length() == 0 || key == null || key.length() == 0 || replaceValue == null)
            return originalText;
        int fromIndex = 0, idx, len = key.length();
        StringBuffer sb = new StringBuffer();
        boolean found = false;
        while ((idx = originalText.indexOf(key, fromIndex)) != -1) {
            if (!found){
                found = true;
            }
            sb.append(originalText.substring(fromIndex, idx));
            sb.append(replaceValue);
            fromIndex = idx + len;
        }
        if (!found)
            return originalText;
        if (fromIndex < originalText.length()) {
            sb.append(originalText.substring(fromIndex));
        }
        return sb.toString();
    }
    
    public static void main(String[] args) throws Exception {
        
        String SQL = "select * from table a wher a.id = '{0}' and a.name = '{1}' ";
        Object[] inputs = new Object[]{1, "a's "};
        inputs = checkSQL(SQL, inputs);
        
        for (int i = 0; i < inputs.length; i++) {
            System.out.println(i + "th: " + inputs[i]);
        }
        
        
    }
    
}