package com.yung.jpa;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.math.BigDecimal;

/**
 * The custom annotation for POJOReprotBean field
 * 
 * @author Yung Long Li
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
// on field level
public @interface HashIndex {
    
    int decimalScale() default 4;
    
    int mode() default BigDecimal.ROUND_HALF_UP;
    
}