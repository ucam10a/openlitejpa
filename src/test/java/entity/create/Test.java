package entity.create;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import com.yung.jpa.AppLogger;
import com.yung.jpa.tool.Attribute;
import com.yung.jpa.tool.EntityOBJ;

public class Test {

    private static final AppLogger logger = AppLogger.getLogger(Test.class);
    
    public static Connection getConnection(String driver, String user, String password, String url) {
        try {
            try {
                Class.forName(driver);
            } catch (Exception e) {
                logger.error(e.toString(), e);
                return null;
            }
            Connection conn = DriverManager.getConnection(url, user, password);
            return conn;
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }
    
    public static void main(String[] args) throws SQLException {
        
//        String tableName = "SCORM_URL";
//        Connection conn = getConnection("org.hsqldb.jdbcDriver", "sa", "", "jdbc:hsqldb:hsql://localhost/scorm");
//        List<Attribute> attrs = EntityOBJ.getAttributes(conn, "PUBLIC", tableName);
//        
//        EntityOBJ entity = new EntityOBJ(EntityOBJ.createClassName(tableName), tableName, attrs);
//        System.out.println(entity.generateCode());
        
    }

}
